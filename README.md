# cosc519-Group3-FinalProject

Group project for Spring 2021 COSC519 at Towson University

To install the command line quota tool (**bold** text are the commands to type):<br/>
Type the following commands into the terminal:<br/>
(1) Type **sudo apt update**<br/>
(2) Type **sudo apt install quota**<br/>
(3) Type **quota --version**<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;- This will result in the text "Quota utilities version 4.05". It may say a newer version if one becomes available.<br/>
(4) Type **sudo apt install linux-image-extra-virtual**<br/>
(5) Type **find /lib/modules/`uname -r` -type f -name '*quota_v*.ko*'**<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;- This will result in two files being displayed, "quota_v1.ko" and "quota_v2.ko". It will also show the kernel version<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;in the path but the actual version does not matter as long as those two files are present.<br/>
(6) Type **sudo nano /etc/fstab**<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;- copy the line with the UUID of the drive (may also have '/', 'ext4', 'errors=remount-ro') and then comment it out wiht a #<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;- paste the copied line below it and type 'usrquota,grpquota' after 'ext4' (here it would replace 'errors=remount-ro')<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;- save the file and exit nano<br/>
(7) Type **sudo mount -o remount**<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;- If "mount: /etc/fstab: parse error" appears, remove and spaces in the 'usrquota,grpquota' text in step 6<br/>
(8) Type **sudo cat /proc/mounts | grep ' / '**<br/>
(9) Type **sudo quotacheck -ugm /**<br/>
(10) Change to the root directory (**cd /**)<br/>
(11) Type **ls**<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;- The two files that should be there are "aquota.group" and "aquota.user"<br/>
(12) Download the files "main.c", "menu.h", and "Makefile" from this repository<br/>
(13) Go to the folder that contains the files downloaded in step 12<br/>
(14) Type **make**<br/>
(15) Type **./main** to run the command line quota tool<br/>
