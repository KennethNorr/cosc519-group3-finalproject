CC=gcc
CFLAG=-g

main: main.c
	$(CC) $(CFLAGS) -o main main.c
	
clean:
	rm -rf main
