// Standard libraries
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include "menu.h"

void print_menu();

int main()
{
	printf("Welcome to an ext4 command line quotatool that uses the package "
		"that can be installed quota.\n");

	char input[25];

	/** User input to call functions **/

	while (strcmp(input, "8") != 0)
	{
		print_menu();

		printf("\nSelection: ");
		scanf("%s", input);

		for (int i = 0; i < strlen(input); i++)
		{
			if (!isdigit(input[i]))
			{
				memset(input, 0, sizeof(input));
				break;
			}
		}

		if (strcmp(input, "1") == 0)
		{
			on_off_menu();
			get_on_off_info();
		}
		else if (strcmp(input, "2") == 0)
		{
			set_quota_menu();
			get_set_info("on");
		}
		else if (strcmp(input, "3") == 0)
		{
			report_menu();
			get_report_info("", "");
		}
		else if (strcmp(input, "4") == 0)
		{
			get_grace_period_menu();
			set_grace_period();
		}
		else if (strcmp(input, "5") == 0)
		{
			get_disable_info();
		}
		else if (strcmp(input, "6") == 0)
		{
			get_manedit_info();
		}
		else if (strcmp(input, "7") == 0)
		{
			get_usernames();
		}
		else if (strcmp(input, "8") == 0)
		{
			printf("Thank you, goodbye\n");
		}
		else
		{
			//this covers '0' also (not a number or out of selection range)
			printf("Invalid selection\n");
		}
	}
}

void print_menu()
{
	printf("\nMenu:\n(1)Toggle on/off quotas\n"
		"(2)Set Quotas by user\n"
		"(3)Report current quotas\n"
		"(4)Set grace period\n"
		"(5)Turn off quotas by user\n"
		"(6)Manually edit quotas by user\n"
		"(7)Get usernames\n"
		"(8)Exit");
}
