#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <pwd.h>

/**
 * Not correct having functionalilty inside declarations for header files
 */

#define CMD_SIZE 2200
#define PATH_SIZE 50
#define USERNAME_SIZE 50
#define _ERROR_COLOR_ "\x1b[31m" // red
#define _RESET_COLOR_ "\x1b[0m" // white

int send_command(const char *command);
int do_toggleQuota(const char *on_off, char *path);
void get_on_off_info();
int do_reportQuotas(char *which_user, char *path);
void get_grace_period_menu();
void set_grace_period();
void set_quota_menu();
void on_off_menu();
void report_menu();
void get_report_info(const char *user, char *path);
void get_disable_info();
void get_manedit_info();
void get_set_info(const char *on_off);
void get_usernames();
unsigned long long convert_input_to_long(const char *value);

void set_quota_menu()
{
	printf("\nSet user quota as the following "
	       "[user] [softlimit] [hardlimit] [path]\n");
}

void on_off_menu()
{
	printf("\nTurn on/off quotas as the following "
		"[on/off] [path]\n");
}

void get_grace_period_menu()
{
	printf("\nSetting grace period for all users in days\n");
}

void set_grace_period()
{
	// Gets information from user about the grace periods and sets them
	// The grace periods are global and are for blocks and inodes, not
	// soft and hard limits. The grace periods apply to the hard limits
	// only... no reason to apply them to soft limits

	int sec_days = 86400;
	char cmd[CMD_SIZE];
	char action[50] = "sudo setquota -t"; // beginning of command
	int bg_int; // holds block grace period as integer
	int ig_int; // holds inode grace period as integer
	char bg_chr[50]; // holds block grace period as characters
	char ig_chr[50]; // holds inode grace perios as characters
	char ph[PATH_SIZE]; // holds path string

	printf("Block grace period: ");
	scanf("%d", &bg_int); // get block grace period from user
	bg_int = bg_int * sec_days;
	printf("Inode grace period: ");
	scanf("%d", &ig_int); // get inode grace period from user
	ig_int = ig_int * sec_days;
	printf("Path: ");
	scanf("%s", ph); // get path from user

	// convert integer grace periods to characters
	sprintf(bg_chr, "%d", bg_int);
	sprintf(ig_chr, "%d", ig_int);

	// create full command and send
	sprintf(cmd, "%s %s %s %s\n", action, bg_chr, ig_chr, ph);
	send_command(cmd);
}

void report_menu()
{
	printf("\nReport quotas for drive as the following "
		"[username] [path]\n");
}

void get_report_info(const char *user, char *path)
{
	char us[USERNAME_SIZE]; // holds user name to get info about
	char ph[PATH_SIZE]; // holds path to get info about

	// If the username and path are supplied, just set the us and
	// ph arrays to user and path, respectively. Otherwise, get
	// the information from the user.
	if ((strcmp(user, "") == 0) && (strcmp(path, "") == 0))
	{
		printf("Which user (username or 'all'): ");
		scanf("%s", us); // get username
		printf("Path: ");
		scanf("%s", ph); // get path
	}
	else
	{
		strcmp(us, user);
		strcmp(ph, path);
	}
	do_reportQuotas(us, ph); // send this information to do_reportQuotas
}

void get_on_off_info()
{
	// This will turn quotas for a path on or off

	char cmd[CMD_SIZE]; // holds command
	char on_off[3]; // holds on/off option
	char ph[PATH_SIZE]; // holds path

	printf("on/off: ");
	scanf("%s", on_off); // get either "on" or "off"
	printf("Path: ");
	scanf("%s", ph); // get path to turn qoutas on or off
	do_toggleQuota(on_off, ph); // send information to do_toggleQuota
}

void get_disable_info()
{
	// This will disable quotas for a user/path combination by setting all
	// quotas (block soft, block hard, inode soft, and inode hard) to zero.

	char cmd[CMD_SIZE];
	char action[50] = "sudo setquota -u"; // beginning of command
	char settings[10] = "0 0 0 0"; // quota settings for all limits
	char us[USERNAME_SIZE]; // holds username
	char ph[PATH_SIZE]; // holds path

	printf("\nUser: ");
	scanf("%s", us); // get username
	printf("Path: ");
	scanf("%s", ph); // get path
	sprintf(cmd, "%s %s %s %s", action, us, settings, ph); // create command string
	send_command(cmd); // send command string

	// automatically output the quota status for the user whose quota has been disabled
	printf("Current quota status for user %s:\n", us);
	get_report_info(us, ph); // get_report_info will show info for user 'us' and path 'ph'
}

void get_manedit_info()
{
	// This option will allow the admin to manually set quotas for a user. This could
	// invite errors since there is no error checking, but some people prefer this method

	char cmd[CMD_SIZE];
	char us[USERNAME_SIZE]; // hold username
	char action[50] = "sudo edquota "; // beginning of command

	printf("\nUser: ");
	scanf("%s", us); // get username to manually set quotas for
	sprintf(cmd, "%s %s", action, us);
	send_command(cmd); // send command to open user's quota specs for editing
}

void get_usernames()
{
	// This will get all of the usernames that have accounts on the system.
	// It accesses the /etc/passwd file for this information. Accounts that
	// we are interested in start at user ID = 1000 and could potentially
	// go to user ID = 60000.

	struct passwd *pw;

	for(int i = 1000; i < 60000; i++)
	{
		// user IDs can range from 1000 to 60000
		pw = getpwuid(i); // gets the passwd structure associated with UID (i)
		if (pw != NULL)
			// non-NULL means there is a valid user
			printf("%s\n", pw->pw_name);
		else
			// NULL value means there are no more users
			break;
	}
}

void get_set_info(const char *on_off)
{
	char cmd[CMD_SIZE]; // holds command
	char mnt[10] = "0 0"; // these are the inode limits (0 = disabled)
	char action[50] = "sudo setquota -u"; // beginning of command
	char us[USERNAME_SIZE]; // holds username
	char sl[15]; // holds character representation of soft limit (e.g. 500M)
	char hl[15]; // holds character representation of hard limit (e.g. 510M)
	char ph[PATH_SIZE]; // holds path
	unsigned long long sl_long; // holds integer representation of soft limit (bytes)
	unsigned long long hl_long; // holds integer representation of hard limit (bytes)

	printf("User: ");
	scanf("%s", us); // get username
	printf("Soft Limit: ");
	scanf("%s", sl); // get soft limit
	printf("Hard Limit: ");
	scanf("%s", hl); // get hard limit
	printf("Path: ");
	scanf("%s", ph); // get path

	sl_long = convert_input_to_long(sl); // convert soft limit (char) to soft limit (long)
	hl_long = convert_input_to_long(hl); // convert hard limit (char) to hard limit (long)
	if (hl_long > sl_long) // make sure hard limit is greater than soft limit
	{
		// limits are okay
		sprintf(cmd, "%s %s %s %s %s %s", action, us, sl, hl, mnt, ph);
		send_command(cmd);
	}
	else
	{
		// hard limit is less than soft limit... do not accept them
		printf(_ERROR_COLOR_"Soft limit of %s is greater than hard limit of %s.\n"_RESET_COLOR_, sl, hl);
		printf(_ERROR_COLOR_"Quotas will not be changed.\n"_RESET_COLOR_);
	}
	// automatically report quota status for user, even if the limits were not changed
	printf("Current quota status for user %s:\n", us);
	do_reportQuotas(us, ph);
}

int do_toggleQuota(const char *on_off, char *path)
{
	char cmd[CMD_SIZE]; // holds command

	printf("Toggle quota for drive %s %s.\n", path, on_off);

	if (strcmp(on_off, "on") == 0)
	{
		// create and send command to turn quotas on for path 'path'
		strcpy(cmd, "sudo quotaon -v ");
		strcat(cmd, path);
		send_command(cmd);
	}
	else if (strcmp(on_off, "off") == 0)
	{
		// create and send command to turn quotas off for path 'path'
		strcpy(cmd, "sudo quotaoff -v ");
		strcat(cmd, path);
		send_command(cmd);
	}
	else
	{
		//nothing here
	}

	return 0;
}

int do_reportQuotas(char *which_user, char *path)
{
	// This will report the quotas for either a single user (which_user = username)
	// or all users (which_user = all) for path 'path'. Note that when reporting
	// all users, EVERY account will be there, not just the accounts with user ID
	// between 1000 and 60000 (see comments in get_usernames).

	char cmd[CMD_SIZE]; // holds command

	if (strcmp(which_user, "all") == 0)
	{
		// report quota information for all accounts
		strcpy(cmd, "sudo repquota -s ");
		strcat(cmd, path);
	}
	else
	{
		// report quota information for account 'which_user' only
		strcpy(cmd, "sudo quota -vs ");
		strcat(cmd, which_user);
	}

	send_command(cmd);

	return 0;
}

unsigned long long convert_input_to_long(const char *value)
{
	// This will convert a number that is in the character format <number><multiplier>
	// (e.g. 500M = 500 Megabytes = 500 * 10^6) to the equivalent integer (long) value.
	// Accepted values for <multiplier>:
	//	K or k = Kilobytes
	//	M or m = Megabytes
	//	G or g = Gigabytes
	//	T or t = Terabytes

	char num[sizeof(value)]; // holds the numerical value (0-9 written to this array)
	char multi[1]; // holds the multiplier value (accepted values are above)
	int j = 0; // used for the num array
	unsigned long value_long; // resulting integer value

	for (int i = 0; i < sizeof(value); i++)
	{
		// go through each character and determine whether or not it is a digit
		if (!isdigit(value[i]))
		{
			// if not a digit, the character is the multiplier
			// there is only one character for multiplier at the end of value
			multi[0] = value[i];
			break;
		}
		else
		{
			// copy all of the digits in value to num
			num[j] = value[i];
			j++;
		}
	}

	value_long = atoi(num); // convert num from characters to an unsigned long long integer

	switch (multi[0])
	{
		// apply multiplier
		case 'K':
			// Kilobytes
			value_long = value_long * (2 ^ 10);
			break;
		case 'M':
			// Megabytes
			value_long = value_long * (2 ^ 20);
			break;
		case 'G':
			// Gigabytes
			value_long = value_long * (2 ^ 30);
			break;
		case 'T':
			// Terabytes
			value_long = value_long * (2 ^ 40);
			break;
		default:
			// just leave value_long as is (no multiplier)
			break;
	}

	return value_long;
}

int send_command(const char *command)
{
	// This sends the command to the host OS... similar to using the command line

	system(command);

	return 0;
}

